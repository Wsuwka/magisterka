#! /usr/bin/env python3

import re
import transposition
import harmonization

comments = []
melody = []
harmony_str = []
header = []


tonality = ""
tonality_level = float(0) # C - 0, Cis = .5 and so on ...


def read(filename):
    with open(filename, "r") as file:
        for line in file.readlines():
            if str(line).startswith("!!!"):
                [ comments.append(item) for item in line ]
            elif str(line).startswith("*"):
                [ header.append(item) for item in line.split("\t") ]
            elif not str(line).startswith("="):
                all_voxes = [str(item) for item in line.split("\t")]
                harmony_str.append(all_voxes)
                melody.append(all_voxes[len(all_voxes) - 1].replace('\n', '')
                              .replace('\\', '')
                              .replace(';', '')
                              .replace('/', '')
                              .replace(':', '')
                              .replace(',', ''))

    tonality, tonality_level = tonality_detector(header)

    harmony = []
    for chord in harmony_str:
        harmony.append(harmonization.read_harmony(chord, tonality_level))

    melody_out = []
    for i in melody:
        tone = []
        regexp = re.compile(r'\d\D')
        if str(i) != '.' and regexp.search(str(i)):
            tone.append(int(i[0]))
            tone.append(transposition.name_to_degree(i[1:], tonality_level))
        # if len(tone) > 0:
        melody_out.append(tone)

    return tonality_level, melody_out, harmony


def tonality_detector(header):
    signs = []
    for i, item in enumerate(header):
        if str(item).startswith("*k["):
            signs.append(str(item).count("-")) # flats count
            signs.append(str(item).count("#")) # crosses count
            break
    if len(signs) > 1:
        if signs[0] > 0:
            # flats
            if signs[0] == 1:
                return "F-dur lub d-moll", 2.5
            elif signs[0] == 2:
                return "B-dur lub g-moll", 5
            elif signs[0] == 3:
                return "Es-dur lub c-moll", 1.5
            elif signs[0] == 4:
                return "As-dur lub f-moll", 4
            elif signs[0] == 5:
                return "Des-dur lub b-moll", .5
            elif signs[0] == 6:
                return "Ges-dur lub es-moll", 3
            elif signs[0] == 7:
                return "Ces-dur lub as-moll", 5.5
            else:
                return "Flats: {0}".format(signs[0])
        if signs[1] > 0:
            # corsses
            if signs[1] == 1:
                return "G-dur lub e-moll", 3.5
            elif signs[1] == 2:
                return "D-dur lub h-moll", 1
            elif signs[1] == 3:
                return "A-dur lub fis-moll", 4.5
            elif signs[1] == 4:
                return "E-dur lub cis-moll", 2
            elif signs[1] == 5:
                return "H-dur lub gis-moll", 5.5
            elif signs[1] == 6:
                return "Fis-dur lub dis-moll", 3
            elif signs[1] == 7:
                return "Cis-dur lub ais-moll", .5
        else:
            return "C-dur lub a-moll", 0
    else:
        return "Unknown tonality..."


# read("melody.krn")
