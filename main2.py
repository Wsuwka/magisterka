#! /usr/bin/env python3

# https://keras.io/getting-started/sequential-model-guide/#compilation

import glob, os

import harmonization
import read_kern
import chord_distance
import numpy as np
from keras.utils import np_utils
from numpy import array
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Activation, Embedding, TimeDistributed
from keras.callbacks import ModelCheckpoint

num_epochs = 30
sequence_length = 2

vocabulary = np.arange(0.0, 6.0, 0.5)
num_steps = 3
batch_size = 2
hidden_size = 50


def main():
    all_harmony_table = harmonization.generate_table_of_function()

    for filename in glob.glob(os.getcwd() + "/kern_files/*.krn"):
        tonality, melody, harmony = read_kern.read(filename)

    # print(melody)
    # print(tonality)
    # print("harmony: " + str(harmony))

    # network_input = []
    # for i in range(0, len(melody)):
    #     sample = []
    #     chord = []
    #     for j in range(len(harmony[i])):
    #         try:
    #             if harmony[i][j][1] >= 0:
    #                 chord.append(harmony[i][j][1])
    #         except IndexError:
    #             pass
    #     if len(chord) > 0 and len(melody[i]) == 2 and melody[i][1] >= 0:
    #         distance = []
    #         for tones, chords, p in all_harmony_table:
    #             distance.append((tones, chord_distance.distance(chords, chord)))
    #         mini = max(all_harmony_table, key=lambda t: t[1])
    #         sample.append(list([melody[i][1], mini[0]]))
    #         if len(sample) > 0:
    #             network_input.append(sample)
    # network_input = array(network_input)

    m = []
    h = []
    for i in range(0, len(melody)):
        if len(melody[i]) > 1:
            m.append(melody[i][1])
        elif i != 0:
            m.append(m[i - 1])
        else:
            m.append(None)

        if len(harmony[i]) > 1:
            h.append(harmony[i][1])
        elif i != 0:
            h.append(h[i - 1][1])
        else:
            h.append(None)

    x_train, x_test = np.array_split(array(m), 2)
    y_train, y_test = np.array_split(array(h), 2)

    model = Sequential()
    model.add(Embedding(len(vocabulary), output_dim=256))
    model.add(LSTM(128))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    model.fit(x_train, y_train, batch_size=16, epochs=10)
    score = model.evaluate(x_test, y_test, batch_size=16)
    print(score)

    # model = Sequential()
    # model.add(Dense(1))
    # model.add(LSTM(
    #     32,
    #     input_shape=(network_input.shape[1], network_input.shape[2])
    #     ,return_sequences=True
    # ))
    # # model.add(Dense(1, activation='sigmoid'))
    # model.add(Dense(1))
    # # model.compile(loss='categorical_crossentropy', optimizer='rmsprop')
    # model.compile(loss='binary_crossentropy', optimizer='rmsprop')
    # checkpointer = ModelCheckpoint(
    #     'weights/model-{epoch:02d}.hdf5', monitor='loss',
    #     verbose=0,
    #     save_best_only=True,
    #     mode='min'
    # )



    # train_data, valid_data, test_data = np.split(network_input, 3)
    # # http://adventuresinmachinelearning.com/keras-lstm-tutorial/
    # print(train_data.shape)
    # print(valid_data.shape)
    # print(test_data.shape)
    #
    # train_data_generator = BatchGenerator(train_data, num_steps, batch_size, vocabulary,
    #                                        skip_step=num_steps)
    # valid_data_generator = BatchGenerator(valid_data, num_steps, batch_size, vocabulary,
    #                                        skip_step=num_steps)
    #
    # model = Sequential()
    # model.add(Embedding(vocabulary, hidden_size, input_length=num_steps))
    # model.add(LSTM(hidden_size, return_sequences=True))
    # model.add(LSTM(hidden_size, return_sequences=True))
    # # if use_dropout:
    # #     model.add(Dropout(0.5))
    # model.add(TimeDistributed(Dense(vocabulary)))
    # model.add(Activation('softmax'))
    # model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['categorical_accuracy'])
    # checkpointer = ModelCheckpoint('weights/model-{epoch:02d}.hdf5', verbose=1)
    # callbacks_list = [checkpointer]
    # model.fit_generator(train_data_generator.generate()
    #                     , len(train_data)//(batch_size*num_steps)
    #                     , num_epochs
    #                     , validation_data=valid_data_generator.generate()
    #                     , validation_steps=len(valid_data)//(batch_size*num_steps)
    #                     , callbacks=callbacks_list
    #                     )
    # filepath = "weights/weights-improvement-{epoch:02d}-{loss:.4f}-bigger.hdf5"
    # checkpoint = ModelCheckpoint(
    #     filepath, monitor='loss',
    #     verbose=0,
    #     save_best_only=True,
    #     mode='min'
    # )
    # callbacks_list = [checkpoint]
    # model.fit(network_input, network_output, epochs=20, batch_size=64, callbacks=callbacks_list)
    # model.fit(network_input, network_output, epochs=20)

    #
    # -------------------------RESULT

    # model = load_model(data_path + "\model-40.hdf5")
    # dummy_iters = 40
    # example_training_generator = BatchGenerator(train_data, num_steps, 1, vocabulary,
    #                                                      skip_step=1)
    # print("Training data:")
    # for i in range(dummy_iters):
    #     dummy = next(example_training_generator.generate())
    # num_predict = 10
    # true_print_out = "Actual words: "
    # pred_print_out = "Predicted words: "
    # for i in range(num_predict):
    #     data = next(example_training_generator.generate())
    #     prediction = model.predict(data[0])
    #     predict_word = np.argmax(prediction[:, num_steps-1, :])
    #     true_print_out += reversed_dictionary[train_data[num_steps + dummy_iters + i]] + " "
    #     pred_print_out += reversed_dictionary[predict_word] + " "
    # print(true_print_out)
    # print(pred_print_out)


# class BatchGenerator(object):
#
#     def __init__(self, data, num_steps, batch_size, vocabulary, skip_step=5):
#         self.data = data
#         self.num_steps = num_steps
#         self.batch_size = batch_size
#         self.vocabulary = vocabulary
#         # this will track the progress of the batches sequentially through the
#         # data set - once the data reaches the end of the data set it will reset
#         # back to zero
#         self.current_idx = 0
#         # skip_step is the number of words which will be skipped before the next
#         # batch is skimmed from the data set
#         self.skip_step = skip_step
#
#     def generate(self):
#         x = np.zeros((self.batch_size, self.num_steps))
#         y = np.zeros((self.batch_size, self.num_steps, self.vocabulary))
#         while True:
#             for i in range(self.batch_size):
#                 if self.current_idx + self.num_steps >= len(self.data):
#                     # reset the index back to the start of the data set
#                     self.current_idx = 0
#                 x[i, :] = self.data[self.current_idx:self.current_idx + self.num_steps]
#                 temp_y = self.data[self.current_idx + 1:self.current_idx + self.num_steps + 1]
#                 # convert all of temp_y into a one hot representation
#                 y[i, :, :] = to_categorical(temp_y, num_classes=self.vocabulary)
#                 self.current_idx += self.skip_step
#             yield x, y


if __name__ == '__main__':
    main()
