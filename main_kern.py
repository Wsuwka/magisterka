#! /usr/bin/env python3

# from plonehrm.notes.notes import Note, Notes
from midiutil import MIDIFile
import weights_generate
import glob, os

import numpy as np
from music21 import converter, instrument, note, chord, stream
from keras.utils import np_utils

from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Activation
from keras.callbacks import ModelCheckpoint

comments = []
melody = []
harmony = []
header = []


tonality = ""
tonality_level = float(0) # C - 0, Cis = .5 and so on ...


def main():
    with open("melody.krn", "r") as file:
        for line in file.readlines():
            if str(line).startswith("!!!"):
                [ comments.append(item) for item in line ]
            elif str(line).startswith("*"):
                [ header.append(item) for item in line.split("\t") ]
            elif not str(line).startswith("="):
                all_voxes = [str(item) for item in line.split("\t")]
                melody.append(all_voxes[len(all_voxes) - 1].replace('\n', '')
                              .replace('\\', '')
                              .replace(';', '')
                              .replace('/', '')
                              .replace(':', ''))

    tonality = tonality_detector(header)
    print("Tonacja -> {0}".format(tonality))

    print("Melody: " + str(melody))


def tonality_detector(header):
    signs = []
    for i, item in enumerate(header):
        if str(item).startswith("*k["):
            signs.append(str(item).count("-")) # flats count
            signs.append(str(item).count("#")) # crosses count
            break
    if len(signs) > 1:
        if signs[0] > 0:
            # flats
            if signs[0] == 1:
                return "F-dur lub d-moll"
            elif signs[0] == 2:
                return "B-dur lub g-moll"
            elif signs[0] == 3:
                return "Es-dur lub c-moll"
            elif signs[0] == 4:
                return "As-dur lub f-moll"
            elif signs[0] == 5:
                return "Des-dur lub b-moll"
            elif signs[0] == 6:
                return "Ges-dur lub es-moll"
            elif signs[0] == 7:
                return "Ces-dur lub as-moll"
            else:
                return "Flats: {0}".format(signs[0])
        if signs[1] > 0:
            # corsses
            if signs[1] == 1:
                return "G-dur lub e-moll"
        else:
            return "C-dur lub a-moll"
    else:
        return "Unknown tonality..."


if __name__ == '__main__':
    main()
