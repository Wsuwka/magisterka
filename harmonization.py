#! /usr/bin/env python3

import transposition
import re


# scale=0 <- dur; scale!=0 <- moll
def generate_table_of_function(scale=0):
    table_of_function = []
    propability = .5
    if scale == 0:
        for i in list([0, 1, 2, 2.5, 3.5, 4.5, 5.5]):
            if i == 2.5 or i == 3.5 or i == 0:
                fun = (i, generate_function(i), propability)
            elif i == 5.5:
                fun = (i, generate_function(i, -1), propability)
            else:
                fun = (i, generate_function(i, 1), propability)
            table_of_function.append(fun)
    else:
        for i in list([4.5, 5.5, 0, 1, 2, 2.5, 4]):
            if i == 0 or i == 2 or i == 2.5:
                fun = (i, generate_function(i), propability)
            elif i == 5.5 or i == 4:
                fun = (i, generate_function(i, -1), propability)
            else:
                fun = (i, generate_function(i, 1), propability)
            table_of_function.append(fun)
    return table_of_function


def generate_function(degree, scale=0):
    if scale == 0:
        return list([degree, (degree+2) % 6, (degree+3.5) % 6])
    elif scale == -1:
        return list([degree, (degree+1.5) % 6, (degree+3) % 6])
    else:
        return list([degree, (degree+1.5) % 6, (degree+3.5) % 6])


def print_table_of_function(table):
    for i in range(0, len(table)):
        print('On degree {0}, we have: '.format(table[i][0])
              + str(transposition.print_name(table[i][1][0])) + ' '
              + str(transposition.print_name(table[i][1][1])) + ' '
              + str(transposition.print_name(table[i][1][2])) + ' ')
        print('\tPropability: ' + str(table[i][2]))


# list of **kern names of tones
def read_harmony(tones, tonality):
    degrees = []
    for tone in tones:
        # x = tone.replace('\n', '').replace('\\', '').replace(';', '').replace('/', '').replace(':', '').replace(',', '')
        # # print(x)
        t = []
        regex_with_signs0 = re.compile(r'\d\D[#-]')
        regex_with_signs00 = re.compile(r'\d\d\D[#-]')
        regex_without_signs0 = re.compile(r'\d\D')
        regex_without_signs00 = re.compile(r'\d\d\D')
        if str(tone) != '.' and regex_with_signs0.search(str(tone)):
            t.append(int(tone[0]))
            t.append(transposition.name_to_degree(tone[1:3], tonality))
        elif str(tone) != '.' and regex_with_signs00.search(str(tone)):
            t.append(int(tone[0:1]))
            t.append(transposition.name_to_degree(tone[2:4], tonality))
        elif str(tone) != '.' and regex_without_signs0.search(str(tone)):
            t.append(int(tone[0]))
            t.append(transposition.name_to_degree(tone[1:2], tonality))
        elif str(tone) != '.' and regex_without_signs00.search(str(tone)):
            t.append(int(tone[0:1]))
            t.append(transposition.name_to_degree(tone[2:3], tonality))
        degrees.append(t)
    return degrees



# what with <h d f>
# t = generate_table_of_function()
# print_table_of_function(t)
# print("=============")
# t2 = generate_table_of_function(1)
# print_table_of_function(t2)


# x = ['4G\\', '4B-\\', '4d/', '4g/\n']
# print(read_harmony(x, 0))
