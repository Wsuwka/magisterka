#! /usr/bin/env python3

#opieramy sie na stopniach gamy!


def tone_to_C(tone, tonality, scale=0):
    # tonality: 1 -> D; -1 -> B
    if scale == 0:
        result = float(tone - tonality)
        # print(str(type(result)) + '' + str(result) + '...')
        if result >= 0:
            return result
        else:
            return float(6.0 + result)
    else:
        result = float(tone - tonality - 3)
        # print(str(type(result)) + '' + str(result) + '...')
        if result >= 0:
            return result
        else:
            return float(6.0 + result)

# def tone_to_a(tone, tonality):
#     return tone - tonality


def name_to_degree(tone, tonality, scale=0):
    if tone == 'C' or tone == 'c':
        return tone_to_C(0, tonality, scale)
    if tone == 'C#' or tone == 'c#' or tone == 'D-' or tone == 'd-':
        return tone_to_C(.5, tonality, scale)
    if tone == 'D' or tone == 'd':
        return tone_to_C(1, tonality, scale)
    if tone == 'D#' or tone == 'd#' or tone == 'E-' or tone == 'e-':
        return tone_to_C(1.5, tonality, scale)
    if tone == 'E' or tone == 'e':
        return tone_to_C(2, tonality, scale)
    if tone == 'F' or tone == 'f':
        return tone_to_C(2.5, tonality, scale)
    if tone == 'F#' or tone == 'f#' or tone == 'G-' or tone == 'g-':
        return tone_to_C(3, tonality, scale)
    if tone == 'G' or tone == 'G':
        return tone_to_C(3.5, tonality, scale)
    if tone == 'G#' or tone == 'g#' or tone == 'A-' or tone == 'a-':
        return tone_to_C(4, tonality, scale)
    if tone == 'A' or tone == 'a':
        return tone_to_C(4.5, tonality, scale)
    if tone == 'A#' or tone == 'a#' or tone == 'H-' or tone == 'h-' or tone == 'b-' or tone == 'B-' or tone == 'b' or tone == 'B':
        return tone_to_C(5, tonality, scale)
    if tone == 'H' or tone == 'h':
        return tone_to_C(5.5, tonality, scale)
    else:
        return -1


def print_name(tone):
    if tone == 0:
        return 'C'
    if tone == .5:
        return 'C#'
    if tone == 1:
        return 'D'
    if tone == 1.5:
        return 'D#'
    if tone == 2:
        return 'E'
    if tone == 2.5:
        return 'F'
    if tone == 3:
        return 'F#'
    if tone == 3.5:
        return 'G'
    if tone == 4:
        return 'G#'
    if tone == 4.5:
        return 'A'
    if tone == 5:
        return 'A#'
    if tone == 5.5:
        return 'H'
    else:
        return ''


# print("B in C-dur is: " + print_name(name_to_degree('A#', 0)))
# print("-----------")
# print("D in D-dur is: " + print_name(name_to_degree('D', 1)))
# print("-----------")
# print("F# in D-dur is: " + print_name(name_to_degree('F#', 1)))
# print("-----------")
# print("-----------")
# print("G# in A-dur is: " + print_name(name_to_degree('G#', 4.5)))
# print("-----------")
# print("E in H-dur is: " + print_name(name_to_degree('E', 5.5)))
# print("-----------")
# print("C in G-dur is: " + print_name(name_to_degree('C', 3.5)))
# print("-----------")
# print("-----------")
# print("g# in a-moll is: " + print_name(name_to_degree('g#', 4.5, 1)))
# print("-----------")
# print("e in h-moll is: " + print_name(name_to_degree('e', 5.5, 1)))
# print("-----------")
# print("c in g-dur is: " + print_name(name_to_degree('c', 3.5, 1)))
