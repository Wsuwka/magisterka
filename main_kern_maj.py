#! /usr/bin/env python3

from music21 import *


def main():
    notes = converter.parse('kern_files/sonate.krn')
    for thisNote in notes.recurse().notes:
      thisNote.addLyric(thisNote.pitch.german)
    notes.show()
    print(notes)


if __name__ == '__main__':
    main()
