#! /usr/bin/env python3

# punishment for 2> 4< or 7?
import math


def distance(base_chord, chord):
    result = 0
    for i in range(0, len(base_chord)):
        for j in chord:
            if i == 0:
                # pryma
                result += tone_distance(base_chord[i], j) * 2
            elif i == 1:
                # tercja
                result += tone_distance(base_chord[i], j) * 1.5
            else:
                result += tone_distance(base_chord[i], j)
    return result


def euclides_tone_distance(base_tone, tone):
    # return math.sqrt(abs(base_tone**2 - tone**2))
    return abs(base_tone - tone)


def tone_distance(tone1, tone2):
    # if distance more than 4 or less than 2>
    if euclides_tone_distance(tone1, tone2) > 2.5 or euclides_tone_distance(tone1, tone2) < 1.5:
        return 0
    # if distance is 3> or 3
    elif euclides_tone_distance(tone1, tone2) >= 1.5 or euclides_tone_distance(tone1, tone2) <= 2.5:
        return 1.0
    # if distance is 4
    else:
        return .5


# print(distance(list([2, 3.5, 4.5]), list([5.5, 4.5, 2])))
# print(distance(list([2, 3.5, 4.5]), list([5.5, 3.5, 2])))
# print(distance(list([2, 3.5, 4.5]), list([4.5, 3.5, 5.5])))
