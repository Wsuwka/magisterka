#! /usr/bin/env python3

# https://towardsdatascience.com/how-to-generate-music-using-a-lstm-neural-network-in-keras-68786834d4c5

# from plonehrm.notes.notes import Note, Notes
from midiutil import MIDIFile
import weights_generate
import glob, os

import numpy as np
from music21 import converter, instrument, note, chord, stream
from keras.utils import np_utils

from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Activation
from keras.callbacks import ModelCheckpoint


def main():
    file = 0
    i = 0
    for filename in glob.glob(os.getcwd() + "/m1/*.mid"):
        print(filename)
        file = filename
        i += 1
        if i == 1:
            break

    notes = []

    midi = converter.parse(file)
    notes_to_parse = None

    parts = instrument.partitionByInstrument(midi)

    if parts: # file has instrument parts
        notes_to_parse = parts.parts[0].recurse()
    else: # file has notes in a flat structure
        notes_to_parse = midi.flat.notes

    for element in notes_to_parse:
        if isinstance(element, note.Note):
            notes.append(str(element.pitch))
        elif isinstance(element, chord.Chord):
            notes.append('.'.join(str(n) for n in element.normalOrder))


    sequence_length = 100

    # get all pitch names
    pitchnames = sorted(set(item for item in notes))

    # create a dictionary to map pitches to integers
    note_to_int = dict((note, number) for number, note in enumerate(pitchnames))

    network_input = []
    network_output = []

    # create input sequences and the corresponding outputs
    for i in range(0, len(notes) - sequence_length, 1):
        sequence_in = notes[i:i + sequence_length]
        sequence_out = notes[i + sequence_length]
        network_input.append([note_to_int[char] for char in sequence_in])
        network_output.append(note_to_int[sequence_out])

    print("--------------")
    print(network_input)
    print("--------------")
    return 0


    n_patterns = len(network_input)

    # reshape the input into a format compatible with LSTM layers
    network_input = np.reshape(network_input, (n_patterns, sequence_length, 1))
    # normalize input
    network_input = network_input / float(len(set(notes)))

    network_output = np_utils.to_categorical(network_output)

    model = Sequential()
    model.add(LSTM(
        256,
        input_shape=(network_input.shape[1], network_input.shape[2]),
        return_sequences=True
    ))
    model.add(Dropout(0.3))
    model.add(LSTM(512, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(256))
    model.add(Dense(256))
    model.add(Dropout(0.3))
    model.add(Dense(len(set(notes))))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    filepath = "weights/weights-improvement-{epoch:02d}-{loss:.4f}-bigger.hdf5"

    checkpoint = ModelCheckpoint(
        filepath, monitor='loss',
        verbose=0,
        save_best_only=True,
        mode='min'
    )
    callbacks_list = [checkpoint]

    model.fit(network_input, network_output, epochs=20, batch_size=64, callbacks=callbacks_list)

    # return network_output, len(set(notes)), pitchnames

    # network_input, voice, pitchnames = weights_generate.main()

    # Generating one line music
    model = Sequential()
    model.add(LSTM(
        512,
        input_shape=(network_input.shape[1], network_input.shape[2]),
        return_sequences=True
    ))
    model.add(Dropout(0.3))
    model.add(LSTM(512, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(512))
    model.add(Dense(256))
    model.add(Dropout(0.3))
    model.add(Dense(len(set(notes))))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    # Load the weights to each node
    # filepath = ""
    # i = 0
    # for filename in glob.glob(os.getcwd() + "/wieghts/*.hdf5"):
    #     filepath = filename
    #     print(filepath)
    #     filepath = filename
    #     i += 1
    #     if i == 1:
    #         break
    model.load_weights("weights/weights-improvement-01-4.1626-bigger.hdf5")

    start = np.random.randint(0, len(network_input)-1)

    int_to_note = dict((number, note) for number, note in enumerate(pitchnames))

    pattern = network_input[start]
    prediction_output = []

    # generate 500 notes
    for note_index in range(500):
        prediction_input = np.reshape(pattern, (1, len(pattern), 1))
        prediction_input = prediction_input / float(len(set(notes)))

        prediction = model.predict(prediction_input, verbose=0)

        index = np.argmax(prediction)
        result = int_to_note[index]
        prediction_output.append(result)

        pattern.append(index)
        pattern = pattern[1:len(pattern)]

    offset = 0
    output_notes = []
    # degrees = [48, 50, 52, 53, 55, 57, 59, 60]
    track    = 0
    channel  = 0
    channel1  = 1
    time     = 0    # In beats
    duration = 1    # In beats
    tempo    = 60   # In BPM
    volume   = 100  # 0-127, as per the MIDI standard

    for pattern in prediction_output:
        # pattern is a chord
        if ('.' in pattern) or pattern.isdigit():
            notes_in_chord = pattern.split('.')
            notes = []
            for current_note in notes_in_chord:
                output_notes.append(int(current_note))
        # pattern is a note
        else:
            output_notes.append(int(pattern))

    MyMIDI = MIDIFile(1)  # One track, defaults to format 1 (tempo track is created
                          # automatically)
    MyMIDI.addTempo(track, time, tempo)

    for i, pitch in enumerate(output_notes):
        MyMIDI.addNote(track, channel, pitch, time + i, duration, volume)
    with open("output.mid", "wb") as output_file:
        MyMIDI.writeFile(output_file)

    # create note and chord objects based on the values generated by the model

    # for pattern in prediction_output:
    #     # pattern is a chord
    #     if ('.' in pattern) or pattern.isdigit():
    #         notes_in_chord = pattern.split('.')
    #         notes = []
    #         for current_note in notes_in_chord:
    #             new_note = note.Note(int(current_note))
    #             new_note.storedInstrument = instrument.Piano()
    #             notes.append(new_note)
    #         new_chord = chord.Chord(notes)
    #         new_chord.offset = offset
    #         output_notes.append(new_chord)
    #     # pattern is a note
    #     else:
    #         new_note = note.Note(pattern)
    #         new_note.offset = offset
    #         new_note.storedInstrument = instrument.Piano()
    #         output_notes.append(new_note)
    #
    #     # increase offset each iteration so that notes do not stack
    #     offset += 0.5
    #
    # midi_stream = stream.Stream(output_notes)
    #
    # midi_stream.write('midi', fp='test_output.mid')


if __name__ == '__main__':
    main()
